# route-api

This is a simple Spring Boot application that exposes data provided
by the [SL Hållplatser och Linjer 2] API in a more user friendly format.

For bus lines, the following can be retrieved:

- The bust lines with the most stops.
- The stops for a specific bus line.

## Requirements

- Java 11
- Maven 3.6
- A Trafiklab/SL API key

## Build

Build the application using Maven:

```sh
$ mvn compile
```

To run the tests:

```sh
$ mvn test
```

## Run

To run the application, create a file named `application.yml` containing the SL API key:

```yaml
sl:
  apikey: <KEY>
```

Then start the application. By default, port 8080 will be used.

```sh
$ mvn spring-boot:run
```

Or, run `RoutesApplication.main()` from the IDE of your choice.

## Usage

Bus lines and bus stops are exposed as follows: 

- `GET lines[?count=N]` - Returns the N bus lines with the most stops (default N=10).   
- `GET lines/{number}` - Returns the bus line, including stops, with the given line number.
- `GET actuator/health` - Returns the health status of the application.

### Examples

Fetching the two longest bus lines:

```
GET /lines?count=2

[
  {
    "number": 636,
    "designation": "636",
    "type": "",
    "stopCount": 232
  },
  {
    "number": 626,
    "designation": "626",
    "type": "",
    "stopCount": 229
  }
]
``` 

Fetching the stops for the longest bus line:

```
GET /lines/636

{
  "number": 636,
  "designation": "636",
  "type": "",
  "stops": [
    {
      "number": 60059,
      "name": "Norrtälje busstation"
    },
    {
      "number": 60061,
      "name": "Älmsta busstation"
    },
    ...
  ]
}
```

Fetching the health status:

```
GET /actuator/health

{"status":"UP"}
```

If the SL API is unavailable, or the key is invalid for example, a status
of `DOWN` will be returned.

## Limitations

- There is limited error handling, for example, if the SL API is unavailable.

- When run in a timezone other than Stockholm (GMT+1), the route updates may not
  be applied as soon as they should.

- The total number of stops for a line includes the number of stops in both directions.
  A stop may thus be counted twice. For example, line 636 has 116 stops, and the  in both directions,
  and thus 232 stops.

  A more suitable way of counting could be to count the maximum number of lines in
  either direction, or the number of distinct stops on the line. 

- Tbe direction of a stop, that is, if a stop is part of the journey from A to B, or the reverse,
  from B to A, is not yet exposed via the API. 

[SL Hållplatser och Linjer 2]: https://www.trafiklab.se/api/sl-hallplatser-och-linjer-2