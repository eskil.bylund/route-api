package com.example.routes;

import io.specto.hoverfly.junit.core.Hoverfly;
import io.specto.hoverfly.junit5.HoverflyExtension;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import static com.example.routes.trafiklab.TestData.lineData;
import static com.example.routes.trafiklab.TestData.simulateFailure;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(HoverflyExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, properties = {"sl.url=http://api.example.com/api2/", "sl.apikey=abc123"})
@DirtiesContext
public class RoutesApplicationFailureTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeAll
    public static void setUp(Hoverfly hoverfly) {
        simulateFailure(hoverfly, lineData);
    }

    @Test
    public void testGetLine() {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/lines/1", String.class);

        assertEquals(503, response.getStatusCodeValue());
    }

    @Test
    public void testLongestLines() {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/lines", String.class);

        assertEquals(503, response.getStatusCodeValue());
    }
}