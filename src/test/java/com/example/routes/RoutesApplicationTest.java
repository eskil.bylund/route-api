package com.example.routes;

import com.example.routes.model.Line;
import com.example.routes.model.LineSummary;
import io.specto.hoverfly.junit.core.Hoverfly;
import io.specto.hoverfly.junit5.HoverflyExtension;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import static com.example.routes.trafiklab.TestData.lineData;
import static com.example.routes.trafiklab.TestData.simulateApi;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(HoverflyExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, properties = {"sl.url=http://api.example.com/api2/", "sl.apikey=abc123"})
@DirtiesContext
public class RoutesApplicationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeAll
    public static void setUp(Hoverfly hoverfly) {
        simulateApi(hoverfly, lineData);
    }

    @Test
    public void testGetLine() {
        ResponseEntity<Line> response = restTemplate.getForEntity("http://localhost:" + port + "/lines/1", Line.class);

        assertEquals(200, response.getStatusCodeValue());

        Line line = response.getBody();

        assertEquals(1, line.getNumber());
        assertEquals("Ö.Ersboda - Vasaplan - Umedalen", line.getDesignation());
        assertEquals(3, line.getStops().size());
    }

    @Test
    public void testLongestLines() {
        ResponseEntity<LineSummary[]> response = restTemplate.getForEntity("http://localhost:" + port + "/lines", LineSummary[].class);

        assertEquals(200, response.getStatusCodeValue());

        LineSummary[] lines = response.getBody();
        assertEquals(4, lines.length);

        LineSummary line = lines[0];
        assertEquals(1, line.getNumber());
        assertEquals("Ö.Ersboda - Vasaplan - Umedalen", line.getDesignation());
        assertEquals(3, line.getStopCount());
    }

    @Test
    public void testLineNotFound() {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/lines/101", String.class);

        assertEquals(404, response.getStatusCodeValue());
        assertTrue(response.getBody().contains("\"error\":\"Not Found\""));
    }
}