package com.example.routes.trafiklab;

import com.google.common.io.Resources;
import io.specto.hoverfly.junit.core.Hoverfly;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.nio.charset.Charset;

import static io.specto.hoverfly.junit.core.SimulationSource.dsl;
import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.serverError;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;

public class TestData {
    public static final String API_KEY = "abc123";
    public static final String lineData;
    public static final String updatedLineData;
    public static final String stopData;
    public static final String pointData;

    static {
        try {
            lineData = readResource("lines.json");
            updatedLineData = readResource("lines-updated.json");
            stopData = readResource("stops.json");
            pointData = readResource("points.json");
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static void simulateApi(Hoverfly hoverfly, String lineData) {
        hoverfly.simulate(dsl(service("api.example.com")
                .get("/api2/LineData.json")
                .queryParam("model", "Line")
                .queryParam("key", API_KEY)
                .queryParam("DefaultTransportModeCode", "BUS")
                .willReturn(success(lineData, MediaType.APPLICATION_JSON_VALUE))

                .get("/api2/LineData.json")
                .queryParam("model", "StopPoint")
                .queryParam("key", API_KEY)
                .queryParam("DefaultTransportModeCode", "BUS")
                .willReturn(success(stopData, MediaType.APPLICATION_JSON_VALUE))

                .get("/api2/LineData.json")
                .queryParam("model", "JourneyPatternPointOnLine")
                .queryParam("key", API_KEY)
                .queryParam("DefaultTransportModeCode", "BUS")
                .willReturn(success(pointData, MediaType.APPLICATION_JSON_VALUE))));
    }

    public static void simulateFailure(Hoverfly hoverfly, String lineData) {
        hoverfly.simulate(dsl(service("api.example.com")
                .get("/api2/LineData.json")
                .anyQueryParams()
                .willReturn(serverError())));
    }

    public static void simulateInvalidKey(Hoverfly hoverfly) {
        hoverfly.simulate(dsl(service("api.example.com")
                .get("/api2/LineData.json")
                .anyQueryParams()
                .willReturn(success("{\"StatusCode\":1002,\"Message\":\"Key is invalid\"}", MediaType.APPLICATION_JSON_VALUE))));
    }

    private static String readResource(String name) throws IOException {
        return Resources.toString(Resources.getResource(name), Charset.defaultCharset());
    }
}
