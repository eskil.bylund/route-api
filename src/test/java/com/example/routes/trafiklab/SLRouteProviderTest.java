package com.example.routes.trafiklab;

import com.example.routes.ServiceUnavailable;
import com.example.routes.model.Line;
import io.specto.hoverfly.junit.core.Hoverfly;
import io.specto.hoverfly.junit5.HoverflyExtension;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.HttpProxy;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.JettyClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

import static com.example.routes.trafiklab.TestData.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(HoverflyExtension.class)
public class SLRouteProviderTest {

    private SLRouteProvider provider;

    @BeforeEach
    public void setUp(Hoverfly hoverfly) {
        SslContextFactory.Client sslContextFactory = new SslContextFactory.Client();
        HttpClient httpClient = new HttpClient(sslContextFactory);
        httpClient.getProxyConfiguration().getProxies().add(new HttpProxy("localhost", hoverfly.getHoverflyConfig().getProxyPort()));

        ClientHttpConnector connector = new JettyClientHttpConnector(httpClient);
        WebClient client = WebClient.builder()
                .baseUrl("http://api.example.com/api2/")
                .clientConnector(connector)
                .build();

        this.provider = new SLRouteProvider(client, API_KEY);
    }

    @Test
    public void testInit(Hoverfly hoverfly) {
        simulateApi(hoverfly, lineData);
        assertFalse(provider.hasRoutes());
        assertNull(provider.getRoutes());

        provider.init();
        hoverfly.verifyAll();

        assertTrue(provider.hasRoutes());
        assertEquals(4, provider.getRoutes().size());
    }

    @Test
    public void testReload(Hoverfly hoverfly) {
        simulateApi(hoverfly, lineData);
        provider.init();
        hoverfly.reset();

        simulateApi(hoverfly, updatedLineData);
        provider.reloadRoutes();
        hoverfly.verifyAll();

        assertTrue(provider.hasRoutes());
        assertEquals(5, provider.getRoutes().size());
    }

    @Test
    public void testGetLine(Hoverfly hoverfly) {
        simulateApi(hoverfly, lineData);
        provider.init();

        assertTrue(provider.getLine(100).isEmpty());

        Line line = provider.getLine(1).get();
        assertEquals(1, line.getNumber());
        assertEquals("Lokaltrafiken", line.getType()); // Ö.Ersboda - Vasaplan - Umedalen
        assertEquals(3, line.getStops().size());
    }

    @Test
    public void testLongestLine(Hoverfly hoverfly) {
        simulateApi(hoverfly, lineData);
        provider.init();

        List<Line> longest = provider.getLongestLines(2);
        assertEquals(2, longest.size());

        assertEquals(1, longest.get(0).getNumber());
        assertEquals(2, longest.get(1).getNumber());
    }

    @Test
    public void testLongestLimits(Hoverfly hoverfly) {
        simulateApi(hoverfly, lineData);
        provider.init();

        assertEquals(0, provider.getLongestLines(0).size());
        assertEquals(4, provider.getLongestLines(100).size());
    }

    @Test
    public void testInvalidApiKey(Hoverfly hoverfly) {
        simulateInvalidKey(hoverfly);
        provider.init();

        assertThrows(ServiceUnavailable.class, () -> provider.getLine(1));
        assertThrows(ServiceUnavailable.class, () -> provider.getLongestLines(10));
    }
}
