package com.example.routes.trafiklab;

import com.example.routes.UnexpectedResponse;
import com.example.routes.model.Line;
import com.example.routes.model.StopPoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Map;

import static com.example.routes.trafiklab.TestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LineDataParserTest {

    private LineDataParser parser;

    @BeforeEach
    public void setUp() {
        this.parser = new LineDataParser();
    }

    @Test
    public void testParse() throws IOException, UnexpectedResponse {
        Map<Integer, Line> lines = parser.parse(lineData, stopData, pointData);
        assertEquals(4, lines.size());

        Line line = lines.get(1);
        assertEquals("Ö.Ersboda - Vasaplan - Umedalen", line.getDesignation());
        assertEquals("Lokaltrafiken", line.getType());
        assertEquals(3, line.getStops().size());

        StopPoint point = line.getStops().get(0);
        assertEquals(10001, point.getNumber());
        assertEquals("Ö.Ersboda", point.getName());
    }
}