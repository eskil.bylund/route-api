package com.example.routes.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A stop on a specific line.
 */
public class StopPoint {
    private final int number;
    private final String name;

    @JsonCreator
    public StopPoint(@JsonProperty("number") int number, @JsonProperty("name") String name) {
        this.number = number;
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }
}
