package com.example.routes.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * A bus line.
 */
public class Line {
    private final int number;
    private final String designation;
    private final String type;
    private final List<StopPoint> stops;

    @JsonCreator
    public Line(@JsonProperty("number") int number, @JsonProperty("designation") String designation,
                @JsonProperty("type") String type, @JsonProperty("stops") List<StopPoint> stops) {

        this.number = number;
        this.designation = designation;
        this.type = type;
        this.stops = new ArrayList<>(stops);
    }

    public int getNumber() {
        return number;
    }

    public String getDesignation() {
        return designation;
    }

    public String getType() {
        return type;
    }

    public List<StopPoint> getStops() {
        return stops;
    }
}
