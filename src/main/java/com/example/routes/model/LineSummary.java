package com.example.routes.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Summary of the information on a line.
 */
public class LineSummary {
    private final int number;
    private String designation;
    private final String type;
    private final int stopCount;

    @JsonCreator
    public LineSummary(@JsonProperty("number") int number, @JsonProperty("designation") String designation,
                       @JsonProperty("type") String type, @JsonProperty("stopCount") int stopCount) {

        this.number = number;
        this.designation = designation;
        this.type = type;
        this.stopCount = stopCount;
    }

    public int getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }

    public int getStopCount() {
        return stopCount;
    }

    public String getDesignation() {
        return designation;
    }
}
