package com.example.routes;

import com.example.routes.model.Line;

import java.util.List;
import java.util.Optional;

/**
 * Responsible for providing line/route information.
 */
public interface RouteProvider {
    /**
     * Returns the line with the given line number.
     */
    Optional<Line> getLine(int lineNumber);

    /**
     * Returns the lines with the most stops on their route.
     *
     * @param count The maximum number of lines to return.
     * @return The N longest lines.
     */
    List<Line> getLongestLines(int count);
}
