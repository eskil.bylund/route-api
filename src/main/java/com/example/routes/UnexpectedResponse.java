package com.example.routes;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Thrown if the route provider fails to parse the content
 * retrieved from the data source.
 */
@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
public class UnexpectedResponse extends Exception {
    public UnexpectedResponse(String message) {
        super(message);
    }

    public UnexpectedResponse(String message, Throwable cause) {
        super(message, cause);
    }
}
