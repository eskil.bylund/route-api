package com.example.routes;

import com.example.routes.model.Line;
import com.example.routes.model.LineSummary;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class LineController {

    private final RouteProvider provider;

    public LineController(RouteProvider provider) {
        this.provider = provider;
    }

    @GetMapping("/lines")
    public List<LineSummary> getLines(@RequestParam(value = "count", defaultValue = "10") int count) {
        return provider.getLongestLines(count).stream()
                .map(LineController::toLineSummary)
                .collect(Collectors.toList());
    }

    @GetMapping("/lines/{id}")
    public Line getLine(@PathVariable int id) {
        Optional<Line> line = provider.getLine(id);
        if (line.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "line not found");
        }
        return provider.getLine(id).get();
    }

    private static LineSummary toLineSummary(Line line) {
        return new LineSummary(line.getNumber(), line.getDesignation(), line.getType(), line.getStops().size());
    }
}