package com.example.routes.trafiklab;

import com.example.routes.UnexpectedResponse;
import com.example.routes.model.Line;
import com.example.routes.model.StopPoint;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Parsers the Trafiklab LineData.json model into the models
 * used by this application.
 */
public class LineDataParser {

    private static final String TRANSPORT_MODE = "BUS";
    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * Parses the Line and JourneyPatternPointOnLine JSON models
     * into line models.
     *
     * @param lineData     The Line JSON data.
     * @param stopData     The StopPoint JSON data.
     * @param pointsOnLine The JourneyPatternPointOnLine JSON data.
     * @return All lines from the given data.
     */
    public Map<Integer, Line> parse(String lineData, String stopData, String pointsOnLine)
            throws JsonProcessingException, UnexpectedResponse {

        ApiResponse lineResponse = mapper.readValue(lineData, ApiResponse.class);
        ApiResponse stopResponse = mapper.readValue(stopData, ApiResponse.class);
        ApiResponse pointResponse = mapper.readValue(pointsOnLine, ApiResponse.class);

        if (lineResponse.statusCode != 0) {
            throw new UnexpectedResponse("Lines: Expected status code 0, got " + lineResponse.statusCode);
        }
        if (stopResponse.statusCode != 0) {
            throw new UnexpectedResponse("Stops: Expected status code 0, got " + stopResponse.statusCode);
        }
        if (pointResponse.statusCode != 0) {
            throw new UnexpectedResponse("Points: Expected status code 0, got " + pointResponse.statusCode);
        }

        assert ("JourneyPatternPointOnLine".equals(pointResponse.responseData.type));

        Map<Integer, Line> lines = parseLines(lineResponse);
        Map<Integer, StopPoint> stops = parseStopPoints(stopResponse);

        for (Model model : pointResponse.responseData.result) {
            if (model.lineNumber == null) {
                continue;
            }
            Line line = lines.get(model.lineNumber);
            if (line == null) {
                continue;
            }
            StopPoint point = stops.get(model.journeyPatternPointNumber);
            if (point != null) {
                line.getStops().add(point);
            }
        }
        return lines;
    }

    private Map<Integer, Line> parseLines(ApiResponse lineResponse) {
        assert ("Line".equals(lineResponse.responseData.type));

        Map<Integer, Line> lines = new HashMap<>();
        for (Model model : lineResponse.responseData.result) {
            if (model.lineNumber == null) {
                continue;
            }
            if (!TRANSPORT_MODE.equals(model.defaultTransportModeCode)) {
                continue;
            }
            Integer lineNumber = model.lineNumber;
            String designation = model.lineDesignation;
            String type = model.defaultTransportMode;
            List<StopPoint> stops = new ArrayList<>();

            Line line = lines.get(lineNumber);
            if (line == null) {
                line = new Line(lineNumber, designation, type, stops);
                lines.put(lineNumber, line);
            }
        }
        return lines;
    }

    private Map<Integer, StopPoint> parseStopPoints(ApiResponse stopResponse) {
        assert ("StopPoint".equals(stopResponse.responseData.type));

        Map<Integer, StopPoint> stops = new HashMap<>();
        for (Model model : stopResponse.responseData.result) {
            Integer number = model.stopPointNumber;
            String name = model.stopPointName;

            if (number == null) {
                continue;
            }
            stops.put(number, new StopPoint(number, name));
        }
        return stops;
    }

    @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class ApiResponse {
        public Integer statusCode;
        public ResponseData responseData;
    }

    @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class ResponseData {
        public String type;
        public List<Model> result;
    }

    @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Model {
        public Integer lineNumber;
        public String lastModifiedUtcDateTime;
        public String existsFromDate;

        // Line model
        public String lineDesignation;
        public String defaultTransportMode;
        public String defaultTransportModeCode;

        // JourneyPatternPointOnLine model
        public Integer directionCode;
        public Integer journeyPatternPointNumber;

        // StopPoint model
        public Integer stopPointNumber;
        public String stopPointName;
    }
}
