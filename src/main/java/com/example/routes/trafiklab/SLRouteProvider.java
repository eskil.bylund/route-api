package com.example.routes.trafiklab;

import com.example.routes.RouteProvider;
import com.example.routes.ServiceUnavailable;
import com.example.routes.UnexpectedResponse;
import com.example.routes.model.Line;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Provides route information from the SL Stops and Lines 2 API
 * provided by Trafiklab.
 * <p>
 * This provider works by loading the routes once at startup and then once daily,
 * to match the update frequency of the underlying SL API.
 */
public class SLRouteProvider implements RouteProvider {
    private static final Logger log = LoggerFactory.getLogger(SLRouteProvider.class);

    private final WebClient client;
    private final String apiKey;
    private Map<Integer, Line> lines;
    private Exception lastExceptiom;

    public SLRouteProvider(WebClient client, String apiKey) {
        this.client = client;
        this.apiKey = apiKey;
    }

    /**
     * Loads the routes once at startup.
     */
    @PostConstruct
    public void init() {
        loadRoutes();
    }

    /**
     * Reloads the routes from the SL API that according to the documentation updates once a day.
     * "The API portal updates data once a day between 0.00-2.00 every day."
     */
    @Scheduled(cron = "0 0 3 * * *")
    public void reloadRoutes() {
        loadRoutes();
    }

    /**
     * Returns true if routes are ready to be served.
     */
    public boolean hasRoutes() {
        return lines != null;
    }

    public List<Line> getRoutes() {
        if (lines == null) {
            return null;
        }
        return new ArrayList<>(lines.values());
    }

    @Override
    public Optional<Line> getLine(int lineNumber) {
        if (lines == null) {
            throw new ServiceUnavailable("Lines failed to load", lastExceptiom);
        }
        return Optional.ofNullable(lines.get(lineNumber));
    }

    @Override
    public List<Line> getLongestLines(int count) {
        if (lines == null) {
            throw new ServiceUnavailable("Lines failed to load", lastExceptiom);
        }
        List<Line> byStops = new ArrayList<>(lines.values());
        byStops.sort(Comparator.comparingInt((Line l) -> l.getStops().size()).reversed());

        return byStops.subList(0, Math.min(count, byStops.size()));
    }

    private void loadRoutes() {
        log.info("Loading routes");
        Stopwatch sw = Stopwatch.createStarted();
        Mono<String> lineData = client.get()
                .uri(getLineDataUri("Line"))
                .retrieve()
                .bodyToMono(String.class);

        Mono<String> stopData = client.get()
                .uri(getLineDataUri("StopPoint"))
                .retrieve()
                .bodyToMono(String.class);

        Mono<String> pointData = client.get()
                .uri(getLineDataUri("JourneyPatternPointOnLine"))
                .retrieve()
                .bodyToMono(String.class);

        try {
            String lines = lineData.block();
            String stops = stopData.block();
            String points = pointData.block();
            log.info("Loaded routes in {}ms ({} bytes total)", sw.elapsed(TimeUnit.MILLISECONDS),
                    lines.getBytes().length + stops.getBytes().length + points.getBytes().length);
            sw.reset();
            sw.start();

            LineDataParser parser = new LineDataParser();
            this.lines = parser.parse(lines, stops, points);
            log.info("Parsed {} lines in {}ms", this.lines.size(), sw.elapsed(TimeUnit.MILLISECONDS));
        } catch (WebClientException | JsonProcessingException | UnexpectedResponse ex) {
            log.error("Failed to load routes", ex);
            lastExceptiom = ex;
        }
    }

    private Function<UriBuilder, URI> getLineDataUri(String model) {
        return ub -> ub
                .path("LineData.json")
                .queryParam("model", model)
                .queryParam("key", apiKey)
                .queryParam("DefaultTransportModeCode", "BUS")
                .build();
    }
}
