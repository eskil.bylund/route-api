package com.example.routes.trafiklab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class SLRouteProviderHealthIndicator implements HealthIndicator {

    private final SLRouteProvider routeProvider;

    @Autowired
    public SLRouteProviderHealthIndicator(SLRouteProvider routeProvider) {
        this.routeProvider = routeProvider;
    }

    @Override
    public Health health() {
        if (!routeProvider.hasRoutes()) {
            return Health.down().build();
        }
        return Health.up().build();
    }
}
