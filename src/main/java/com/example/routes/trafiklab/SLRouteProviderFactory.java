package com.example.routes.trafiklab;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.HttpProxy;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.JettyClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class SLRouteProviderFactory {
    @Value("${sl.url:https://api.sl.se/api2/}")
    private String url;

    @Value("${sl.apikey}")
    private String apiKey;

    @Bean
    public SLRouteProvider routeProvider() {
        SslContextFactory.Client sslContextFactory = new SslContextFactory.Client();
        HttpClient httpClient = new HttpClient(sslContextFactory);

        String proxyHost = System.getProperty("http.proxyHost");
        String proxyPort = System.getProperty("http.proxyPort");

        // Force the client to use the Java HTTP proxy settings
        if (proxyHost != null && proxyPort != null) {
            httpClient.getProxyConfiguration().getProxies().add(new HttpProxy(proxyHost, Integer.parseInt(proxyPort)));
        }

        ClientHttpConnector connector = new JettyClientHttpConnector(httpClient);
        WebClient client = WebClient.builder()
                .baseUrl(url)
                .clientConnector(connector)
                .codecs(configurer -> configurer
                        .defaultCodecs()
                        .maxInMemorySize(16 * 1024 * 1024))
                .build();

        return new SLRouteProvider(client, apiKey);
    }
}
